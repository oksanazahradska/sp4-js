document.addEventListener('DOMContentLoaded', function () {

    let originalData = DATA.slice();
    let filteredData = originalData.slice();

    function sortByExperience(data) {
        return data.slice().sort((a, b) => parseInt(b.experience) - parseInt(a.experience));
    }

    function sortByLastName(data) {
        return data.slice().sort((a, b) => a["last name"].localeCompare(b["last name"], 'uk'));
    }

    function sortByDefault(data) {
        return data.slice();
    }





    function showGym() {


        let data = originalData.filter(trainer => trainer.specialization === 'Тренажерний зал');
        console.log("Filtered data for Gym:", data);
        return data;

    }

    function showFightClub() {


        let data = originalData.filter(trainer => trainer.specialization === 'Бійцівський клуб');
        console.log("Filtered data for fight:", data);
        return data;
    }

    function showKidsClub() {


        let data = originalData.filter(trainer => trainer.specialization === 'Дитячий клуб');
        console.log("Filtered data for kidsclub:", filteredData);

        return data;
    }
    function showSwimmingPool() {


        let data = originalData.filter(trainer => trainer.specialization === 'Басейн');
        console.log("Filtered data for pool:", filteredData);

        return data;
    }

    function showAll() {

        let data = originalData.slice();
        console.log("Filtered data for all:", filteredData);

        return data;
    }




    function showMaster() {


        let data = originalData.filter(trainer => trainer.category === 'майстер');
        console.log("Filtered data for master:", filteredData);

        return data;
    }
    function showSpecialist() {


        let data = originalData.filter(trainer => trainer.category === 'спеціаліст');
        console.log("Filtered data for specialist:", filteredData);

        return data;
    }
    function showInstructor() {


        let data = originalData.filter(trainer => trainer.category === 'інструктор');
        console.log("Filtered data for instructor:", filteredData);

        return data;
    }

    function showAllTrainers() {

        let data = originalData.slice();
        console.log("Filtered data for all trainers:", filteredData);

        return data;
    }












    const trainerCardTemplate = document.getElementById('trainer-card');
    const trainerCardContent = trainerCardTemplate.content.cloneNode(true);
    const trainerContainer = document.querySelector('.trainers-cards__container');





    function renderItems(data) {


        trainerContainer.innerHTML = '';





        data.forEach(item => {


            const cloneCardContent = trainerCardContent.cloneNode(true);
            const imgSrc = item.photo;
            const name = item["last name"] + "   " + item["first name"];

            const imgTrainer = cloneCardContent.querySelector('.trainer__img');
            imgTrainer.src = imgSrc;


            const nameTrainer = cloneCardContent.querySelector('.trainer__name');
            nameTrainer.textContent = name;






            let showButton = cloneCardContent.querySelectorAll('.trainer__show-more');
            showButton.forEach(button => {

                button.addEventListener('click', function () {

                    let currentItem = item;

                    let modalCard = document.getElementById('modal-template').content.cloneNode(true);

                    const imgH = modalCard.querySelector('.modal__img');
                    const nameH = modalCard.querySelector('.modal__name');
                    const specializationH = modalCard.querySelector('.modal__point--specialization');
                    const categoryH = modalCard.querySelector('.modal__point--category');
                    const experienceH = modalCard.querySelector('.modal__point--experience');
                    const descriptionH = modalCard.querySelector('.modal__text');

                    const imgSrc = currentItem.photo;
                    const name = currentItem["last name"] + "   " + currentItem["first name"];
                    const specialization = currentItem.specialization;
                    const category = currentItem.category;
                    const experience = currentItem.experience;
                    const description = currentItem.description;


                    imgH.src = imgSrc;
                    nameH.textContent = name;
                    specializationH.textContent = specialization;
                    categoryH.textContent = category;
                    experienceH.textContent = experience;
                    descriptionH.textContent = description;

                    document.body.appendChild(modalCard);
                    document.body.style.overflow = 'hidden';


                    const closeButton = document.querySelector('.modal__close');

                    closeButton.addEventListener('click', () => {
                        const modal = document.querySelector('.modal');
                        modal.remove();
                        document.body.style.overflow = 'auto';


                    });






                });

            });






            trainerContainer.appendChild(cloneCardContent);

        });






        const sidebar = document.querySelector('.sidebar');
        const sorting = document.querySelector('.sorting');

        sorting.removeAttribute('hidden');
        sidebar.removeAttribute('hidden');
    }



    const sortingBtn = document.querySelectorAll('.sorting__btn');
    sortingBtn.forEach((button, index) => {
        button.addEventListener('click', function () {
            sortingBtn.forEach(btn => btn.classList.remove('sorting__btn--active'));
            this.classList.add('sorting__btn--active');



            switch (index) {
                case 0:
                    filteredData = sortByDefault(filteredData);
                    break;
                case 1:
                    filteredData = sortByLastName(filteredData);
                    break;
                case 2:
                    filteredData = sortByExperience(filteredData);
                    break;
                default:
                    break;

            }

            renderItems(filteredData);

        });


        const filtersSubmit = document.querySelector('.filters__submit');


        filtersSubmit.addEventListener('click', function (event) {
            event.preventDefault();

    


            const selectedDirection = document.querySelector('input[type="radio"][name="direction"]:checked');
            const selectedCategory = document.querySelector('input[type="radio"][name="category"]:checked');
            if (selectedDirection && selectedCategory) {
                const directionValue = selectedDirection.value;
                const categoryValue = selectedCategory.value;


                switch (directionValue) {
                    case 'gym':

                        filteredData = showGym();

                        break;
                    case 'fight club':
                        filteredData = showFightClub();



                        break;
                    case 'kids club':

                        filteredData = showKidsClub();

                        break;
                    case 'swimming pool':
                        filteredData = showSwimmingPool();

                        break;
                    case 'all':
                        filteredData = showAll();

                        break;
            default:
                break;
        }
        switch (categoryValue){
                
        
            case 'master':
                filteredData = filteredData.filter(trainer => trainer.category === 'майстер');
                break;
            case 'specialist':
                filteredData = filteredData.filter(trainer => trainer.category === 'спеціаліст');
                break;
            case 'instructor':
                filteredData = filteredData.filter(trainer => trainer.category === 'інструктор');
                break;
            case 'all':
                break;
                default:
                    break;

                }


                renderItems(filteredData);



            }





            renderItems(filteredData);
        });
        



    });

    renderItems(originalData);




















});